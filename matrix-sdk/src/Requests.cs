﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Net;

namespace matrix_sdk.src
{
    static class Requests
    {

        private static string accessToken;

        public static string AccessToken
        {
            get
            {
                return accessToken;
            }
            set
            {
                accessToken = value;
            }
         }

        //for unstatic
        //public Requests()
        //{
        //    var login = new Login();
        //    AccessToken = login.GetAuthorizationToken("@spisok-anonymous:matrix.org", "spisokpassword", "m.login.password"); 
        //}

        public static JObject POST(string url, JObject body)
        {
            url += "access_token=" + accessToken;
            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = body.ToString();

                streamWriter.Write(json);
                streamWriter.Flush();
            }

            WebResponse response = request.GetResponse();

            var serializer = new JsonSerializer();
            var responseObject = new JObject();

            using (var streamReader = new StreamReader(response.GetResponseStream()))
            {
                using (var jsonTextReader = new JsonTextReader(streamReader))
                {
                    responseObject = JObject.Parse(serializer.Deserialize(jsonTextReader).ToString());
                }
            }

            return responseObject;
        }

        public static JObject POST(string url, JObject body, string queryParameters)
        {
            return POST(url + queryParameters, body);
        }

        public static JObject GET(string url)
        {
            url += "access_token=" + accessToken;
            WebRequest request = WebRequest.Create(url);
            request.Method = "GET";

            WebResponse response = request.GetResponse();

            var serializer = new JsonSerializer();
            var responseObject = new JObject();

            using (var streamReader = new StreamReader(response.GetResponseStream()))
            {
                using (var jsonTextReader = new JsonTextReader(streamReader))
                {
                    responseObject = JObject.Parse(serializer.Deserialize(jsonTextReader).ToString());
                }
            }

            return responseObject;
        }

        public static JObject GET(string url, string queryParameters)
        {
            return GET(url + queryParameters);
        }
    }
}
