﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Net;

namespace matrix_sdk.src.ClientAuthentication
{
    class Login
    {
        public string GetAuthorizationToken(string user, string password, string type)
        {
            string url = "https://matrix.org/_matrix/client/r0/login";

            JObject body = JObject.FromObject(new
            {
                user = user,
                password = password,
                type = type
            });

            JObject responseObject = POST(url, body);

            return responseObject["access_token"].ToString();
        }

        //post request without authorization
        private JObject POST(string url, JObject body)
        {
            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = body.ToString();

                streamWriter.Write(json);
                streamWriter.Flush();
            }

            WebResponse response = request.GetResponse();

            var serializer = new JsonSerializer();
            var responseObject = new JObject();

            using (var streamReader = new StreamReader(response.GetResponseStream()))
            {
                using (var jsonTextReader = new JsonTextReader(streamReader))
                {
                    responseObject = JObject.Parse(serializer.Deserialize(jsonTextReader).ToString());
                }
            }

            return responseObject;
        }

        //TODO
        //tokenrefresh
        //logout
    }
}
